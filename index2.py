# import modules
from UrbanObservatoryAPI import UrbanObservatoryAPI
import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import pyplot as plt
import random
import math
import tkinter as tk
from functools import partial
import datetime
import time
import json

matplotlib.use("TkAgg")


# main window object
class Root(object):
    def __init__(self, data):
        self.x = []
        self.y = []
        self.line_list = []
        self.current_table = 0
        self.current_sensor = 0
        self.data_struct = data
        self.fig = plt.figure(1)
        self.root = tk.Tk()
        self.canvas = matplotlib.backends.backend_tkagg.FigureCanvasTkAgg(self.fig, master=self.root)
        self.plot_widget = self.canvas.get_tk_widget()
        quant = math.trunc(math.sqrt(self.data_struct.quantity)) + 1
        self.plot_widget.grid(row=quant + 1, column=0, rowspan=4, columnspan=quant)
        counter = -1
        tables = []
        for i in self.data_struct.tables:
            tables.append(i)
        for r in range(quant):
            for c in range(quant):
                counter += 1
                if counter < self.data_struct.quantity:
                    print(counter)
                    action = partial(self.add_plot, self.data_struct.tables[tables[counter]])
                    tk.Button(self.root, text=tables[counter],
                              command=action).grid(row=r, column=c)
        self.sensor_label = tk.Label(self.root, text="Sensor: All", font=("Helvetia", 15))
        self.sensor_label.grid(row=quant + 2, column=quant + 1)
        action = partial(self.press_up_down, True)
        tk.Button(self.root, text="Up", command=action).grid(row=quant + 1, column=quant + 1, padx=150)
        action = partial(self.press_up_down, False)
        tk.Button(self.root, text="Down", command=action).grid(row=quant + 3, column=quant + 1)
        self.root.mainloop()

    def add_plot(self, plot_list):
        self.current_table = plot_list.ID
        self.current_units = plot_list.units
        self.current_sensor = 0
        self.sensor_label.config(text="Sensor:All")
        self.fig.clf()
        # used to obtain the same colours every time without defining the quantity required
        random.seed(365)
        # separate data from the plot_list into x and y coordinate lists
        self.x = []
        self.y = []
        for i in plot_list.sensors:
            self.x.append([])
            self.y.append([])
            for j in plot_list.sensors[i].records:
                stamp = j.timestamp.split(" ")
                stamp[0] = stamp[0].split("-")
                stamp[1] = stamp[1].split(":")
                stamp_value = datetime.datetime(int(stamp[0][0]), int(stamp[0][1]), int(stamp[0][2]),
                                                int(stamp[1][0]), int(stamp[1][1]))
                self.x[-1].append(stamp_value)
                self.y[-1].append(j.value)

        # create axis and plot lines separated by record id
        self.ax = self.fig.add_subplot(1, 1, 1)
        for i in range(len(self.x)):
            self.x[i], self.y[i] = zip(*sorted(zip(self.x[i], self.y[i])))
            self.ax.plot(self.x[i], self.y[i], linewidth=0.5,
                         color=(random.randint(0, 100) / 100,
                                random.randint(0, 100) / 100,
                                random.randint(0, 100) / 100))

        plt.xlabel("time / timestamp")
        plt.ylabel(plot_list.ID + "/" + plot_list.units)
        self.ax.set_title(plot_list.ID)
        self.canvas.draw()

    def press_up_down(self, up):
        if self.current_table != 0:
            if up:
                if self.current_sensor >= self.data_struct.tables[self.current_table].length:
                    self.current_sensor = 0
                else:
                    self.current_sensor += 1
            else:
                if self.current_sensor == 0:
                    self.current_sensor = self.data_struct.tables[self.current_table].length
                else:
                    self.current_sensor -= 1
            print(self.current_sensor)
            self.fig.clf()
            random.seed(365)
            self.ax = self.fig.add_subplot(1, 1, 1)
            current_x = 0
            current_y = 0
            for i in range(len(self.x)):
                if i != self.current_sensor - 1 and self.current_sensor != 0:
                    self.ax.plot(self.x[i], self.y[i], linewidth=0.5, color=(.9, .9, .9))

                    random.randint(0, 100)
                    random.randint(0, 100)
                    random.randint(0, 100)
                elif self.current_sensor == 0:
                    self.ax.plot(self.x[i], self.y[i], linewidth=0.5,
                                 color=(random.randint(0, 100) / 100,
                                        random.randint(0, 100) / 100,
                                        random.randint(0, 100) / 100))
                else:
                    current_x = self.x[i]
                    current_y = self.y[i]
                    c = [random.randint(0, 100) / 100,
                         random.randint(0, 100) / 100,
                         random.randint(0, 100) / 100]
            if self.current_sensor != 0:
                self.ax.plot(current_x, current_y, linewidth=1, color=(c[0], c[1], c[2]))
                new_text = self.data_struct.tables[self.current_table].sensor_list[self.current_sensor - 1].ID
                self.sensor_label.config(text="Sensor:" + new_text)
            else:
                self.sensor_label.config(text="Sensor:All")
            plt.xlabel("time / timestamp")
            plt.ylabel(str(self.current_table) + "/" + str(self.current_units))
            self.ax.set_title(self.current_table)
            self.canvas.draw()


# contains organised data from sensors
class DataStructure(object):
    def __init__(self, data):
        data_store = {}
        for i in data:
            if i["type"] not in data_store:
                data_store[i["type"]] = {}
            if i["sensor_id"] not in data_store[i["type"]]:
                data_store[i["type"]][i["sensor_id"]] = []
            data_store[i["type"]][i["sensor_id"]].append(i)

        self.quantity = len(data_store)
        self.tables = {}
        for i in data_store:
            self.tables[i] = Table(data_store[i], i, data)


# organises the data by type
class Table(object):
    def __init__(self, sensors, sid, data):
        self.ID = sid
        self.sensors = {}
        self.sensor_list = []
        for i in sensors:
            self.sensors[i] = Sensor(sensors[i], i)
            self.sensor_list.append(self.sensors[i])
        self.length = len(self.sensors)
        print(self.length)

        self.records = []
        self.units = None
        for i in data:
            if i["type"] == self.ID:
                self.records.append(Record(i["timestamp"], i["lat"], i["lng"], i["value"]))
                if not self.units:
                    self.units = i['units']


# organises the data by sensor id
class Sensor(object):
    def __init__(self, records, sid):
        self.ID = sid
        self.records = []
        for i in records:
            self.records.append(Record(i["timestamp"], i["lat"], i["lng"], i["value"]))


# contains information for each record
class Record(object):
    def __init__(self, timestamp, lat, lng, value):
        self.timestamp = timestamp
        self.lat = lat
        self.lng = lng
        self.value = value


# obtains data from sensors
api_key = "j9j780sqlfx7am0sgdtxcz2gbs7nuddfvksvhg17kycz0fv3cuwgy9pir2bdaw91mi7dmyqzbridg3mxkixf2w1nlp"
api = UrbanObservatoryAPI(api_key)

dta = api.read_from_api("start_time=20170116120000&end_time=20170117180000",
                        'temp.json').parse_data()
print(dta)

# formats data and puts it into a root object
data_struct = DataStructure(dta)
root = Root(data_struct)
