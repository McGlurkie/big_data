# import libraries
import dataformat  # used to update data
import matplotlib as mpl  # used to plot graphs
import matplotlib.dates as dates
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg  # allows for mpl to link with tk
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import random
from functools import partial  # used to give buttons functions with parameters
import datetime  # creates datetime objects for use as timestamps
from dateutil.relativedelta import relativedelta
import pymongo  # stores the records
import sqlite3  # stores data on the sensors
import odo
import pandas
import base64
from io import BytesIO
from pprint import pprint  # useful for debugging code
import statistics
import threading  # used to thread during data updates
import numpy as np
from stl import mesh
import time
import calendar
import sys
import platform

if platform.system() == "Windows":
    import ctypes
else:
    print("Incompatible OS. Please use Windows")
    sys.exit()
# check current version of python and import based on it
if sys.version[0] == "2":
    import Tkinter as tk  # used to display a window
    from urllib import urlopen
elif sys.version[0] == "3":
    import tkinter as tk
    from urllib.request import urlopen
else:  # if current version of python is incompatible
    print("Version " + sys.version[0] + " of python is not compatible. Use version 2 or 3.")
    sys.exit()
from PIL import Image, ImageTk as ITK
# allows mpl to link with tk
mpl.use("TkAgg")


# main window application
class RootApplication(object):
    def __init__(self):
        """
        :param master: tk.Tk() root object to call the main window

        Note: print statements are used mainly for debugging and will be
              ignored if this program is run as an application.

        Description:
        RootApplication is the main object for the program
        which requires a tk.Tk() object to run. Everything
        to do with the gui displayed is in this class, except
        for the data load text widget which is changed externally.
        """
        self.fsize = 18
        self.gid = None
        self.t = time.time()
        self.bg_color = "#3030FF"
        self.calendar_color = "#F0F0B0"
        self.text_color = "#E0E0E0"
        self.base_url = "https://maps.googleapis.com/maps/api/staticmap?"
        self.api_key = "AIzaSyAwEVZqFw1NiguACzpBFgPpg48YQW7qyBE"
        # variable to allow the window to be called
        self.master = tk.Tk()
        self.master.title("Big Data")
        print(SCREEN_SIZE)
        # set the window position and maximise the screen
        self.master.geometry("+0+0")
        self.master.state("zoomed")
        self.master.configure(bg=self.bg_color)
        # give all rows and columns in the window weight
        for rows in range(2):
            self.master.grid_rowconfigure(rows, weight=1)
        for columns in range(2):
            self.master.grid_columnconfigure(columns, weight=1)

        # frame to contain widgets for the calendar
        self.calendar_frame = tk.Frame(self.master, bg=self.calendar_color, bd=5)
        self.calendar_frame.grid(row=0, column=0, sticky="NSEW", padx=10, pady=10)

        for rows in range(11):
            self.calendar_frame.grid_rowconfigure(rows, weight=1)
        for columns in range(7):
            self.calendar_frame.grid_columnconfigure(columns, weight=1)

        # frame to contain widgets for text
        self.text_frame = tk.Frame(self.master, bg=self.text_color, bd=5)
        self.text_frame.grid(row=1, column=0, columnspan=2, sticky="NSEW", padx=10, pady=10)

        for rows in range(3):
            self.text_frame.grid_rowconfigure(rows, weight=1)
        for columns in range(5):
            self.text_frame.grid_columnconfigure(columns, weight=1)

        # create mpl figure and put it in a tk widget
        self.fig = plt.figure(1)
        self.canvas = mpl.backends.backend_tkagg.FigureCanvasTkAgg(self.fig, master=self.master)
        self.plot_widget = self.canvas.get_tk_widget()
        self.plot_widget.grid(row=0, column=1, sticky="NSEW", padx=10, pady=10)
        self.ax = self.fig.add_subplot(1, 1, 1)
        self.fig.canvas.callbacks.connect('button_press_event', self.button_press)
        self.fig.canvas.callbacks.connect('pick_event', self.on_click)

        # access MongoDB database
        print("Loading Data")
        td = time.time()
        self.client = pymongo.MongoClient("mongodb://pi:3.14159@10.70.39.148/big_database")
        self.db = self.client.big_database
        print("Time to load Data: " + str(time.time() - td))

        # access sqlite database
        self.conn = sqlite3.connect("Data.db")
        self.cursor = self.conn.cursor()

        # organise widgets into their own function
        self.make_widgets()
        self.master.mainloop()

    def make_widgets(self):
        """
        :return: None

        Description:
        This function is to simply have all of the widgets created in one
        place. This can be considered an extension of the __init__ function.
        """

        # text widget to show progress in loading the graph
        self.load_label = tk.Label(self.text_frame, text="----", font=("", self.fsize), bg=self.text_color)
        self.load_label.grid(row=0, column=3, sticky="w")

        # button to save currently displayed graph as stl or png file
        self.save_button = tk.Button(self.text_frame, text="Save as png", command=self.save, font=("", self.fsize))
        self.save_button.grid(row=0, column=4)

        # initial date
        self.year = 2015
        self.month = 1

        # calendar title
        self.calendar_label = tk.Label(self.calendar_frame, text="Calendar",
                                       font=("", self.fsize, "underline"), bg=self.calendar_color)
        self.calendar_label.grid(row=0, column=0, columnspan=7)

        # label to display currently selected year
        self.year_label = tk.Label(self.calendar_frame, text=str(self.year),
                                   font=("", self.fsize), bg=self.calendar_color)
        self.year_label.grid(row=1, column=2, columnspan=3)

        # buttons used to cycle between years
        action = partial(self.new_month_year, -1, True)
        self.lower_year = tk.Button(self.calendar_frame, text="<", command=action)
        self.lower_year.grid(row=1, column=0, columnspan=2)

        action = partial(self.new_month_year, 1, True)
        self.raise_year = tk.Button(self.calendar_frame, text=">", command=action)
        self.raise_year.grid(row=1, column=5, columnspan=2)

        # label to display currently selected month
        self.month_label = tk.Label(self.calendar_frame, text=str(self.month).zfill(2),
                                    font=("", self.fsize), bg=self.calendar_color)
        self.month_label.grid(row=2, column=2, columnspan=3)

        # buttons used to cycle between months
        action = partial(self.new_month_year, -1, False)
        self.lower_month = tk.Button(self.calendar_frame, text="<", command=action)
        self.lower_month.grid(row=2, column=0, columnspan=2)

        action = partial(self.new_month_year, 1, False)
        self.raise_month = tk.Button(self.calendar_frame, text=">", command=action)
        self.raise_month.grid(row=2, column=5, columnspan=2)

        # label used to notify the user of any data being loaded from the API
        self.thread_label = tk.Label(self.text_frame, text="No Data to Load", font=("", self.fsize), bg=self.text_color)
        self.thread_label.grid(row=2, column=3, sticky="w")

        self.day = 0  # currently selected day
        self.calendar_buttons = []  # list of all day buttons
        self.button_ids = calendar.monthcalendar(self.year, self.month)  # list containing days of the month in order
        if len(self.button_ids) == 5:  # preventing the list from only showing 5 weeks
            self.button_ids.append([0, 0, 0, 0, 0, 0, 0])
        f = open("ValidDates.csv", "r")
        self.valid_dates = list(map(self.set_dt, f.read().split(",")))  # accessible dates in the database
        f.close()
        print(self.valid_dates)

        # loop used to create the buttons used to display the days
        for week in range(6):
            self.calendar_buttons.append([])
            for day in range(7):
                # get current date iteration in format YYYY-MM-DD
                try:
                    date = datetime.datetime(self.year, self.month, self.button_ids[week][day])
                except ValueError:
                    date = 0
                # link to function with parameters
                action = partial(self.new_day, week, day)
                # text displayed on the button
                txt = str(self.button_ids[week][day])

                if self.button_ids[week][day] == 0:
                    # create white button with an ignored command
                    self.calendar_buttons[week].append(tk.Button(self.calendar_frame, text="--", command=action,
                                                                 bg="white"))
                    self.calendar_buttons[week][day].configure(state="disabled")
                elif date in self.valid_dates and self.day == 0:
                    # store current date variables
                    self.day = self.button_ids[week][day]
                    self.week_day = day
                    self.week = week
                    # create button as yellow and selected
                    self.calendar_buttons[week].append(tk.Button(self.calendar_frame, text=txt.zfill(2),
                                                                 command=action, bg="yellow", relief="sunken"))
                    self.calendar_buttons[week][day].configure(state="disabled")
                elif date in self.valid_dates and self.day != 0:
                    # display grey button
                    self.calendar_buttons[week].append(tk.Button(self.calendar_frame, text=txt.zfill(2),
                                                                 command=action, bg="grey"))
                else:
                    # create a red button with an ignored command
                    self.calendar_buttons[week].append(tk.Button(self.calendar_frame, command=action, text=txt.zfill(2),
                                                                 bg="red"))
                    self.calendar_buttons[week][day].configure(state="disabled")

                # position the button
                self.calendar_buttons[week][day].grid(row=week + 3, column=day, sticky="NESW")

        # store the currently selected date
        try:
            self.current_date = datetime.datetime(self.year, self.month, self.day)
        except ValueError:
            self.current_date = datetime.datetime(1, 1, 1)
        self.delta = datetime.timedelta(days=1)
        print("Current date:", self.current_date)

        self.collection = self.db[str(self.year) + str(self.month)]

        # display the currently selected date
        self.date_display = tk.Label(self.calendar_frame, text=self.current_date.date(),
                                     font=("", self.fsize), bg=self.calendar_color)
        self.date_display.grid(row=9, column=0, columnspan=7)

        # radio button used to allow the user to see data for a month
        self.month_var = tk.BooleanVar()
        self.month_var.set(False)
        self.month_select = tk.Checkbutton(self.calendar_frame, text="Select Month", command=self.select_month,
                                           bg=self.calendar_color, font=("", self.fsize),
                                           variable=self.month_var, onvalue=True, offvalue=False)
        self.month_select.grid(row=10, column=0, columnspan=7, sticky="NESW")

        # label displayed next to type dropdown list
        self.type_label = tk.Label(self.text_frame, text="Data Type:", font=("", self.fsize), bg=self.text_color)
        self.type_label.grid(row=0, column=0, sticky="e")

        if self.day > 0:  # if data is available for the initial month
            # obtain a list of data types available for the current date
            query = {"timestamp": {"$gte": self.current_date, "$lt": self.current_date + self.delta}}
            self.type_list, self.unit_list = zip(*map(self.get_type,
                                                      self.db.records.find(query, {"type": 1}).distinct("type-unit")))

        else:  # if no data is available
            self.type_list = []
            self.unit_list = []
        print("Type List:")
        print(self.type_list)
        print("Unit List:")
        print(self.unit_list)
        # create dropdown list to select data type
        self.type_var = tk.StringVar()  # variable used to access selected value
        self.type_var.set("None")
        self.type_drop = tk.OptionMenu(self.text_frame, self.type_var, "None",
                                       *self.type_list, command=self.new_type)
        self.type_drop.grid(row=0, column=1)
        self.type_drop.configure(highlightthickness=0)

        # label displayed next to sensor dropdown list
        self.sensor_label = tk.Label(self.text_frame, text="Sensor:", font=("", self.fsize), bg=self.text_color)
        self.sensor_label.grid(row=1, column=0, sticky="e")

        # labels to display global position of the currently selected sensor
        self.location_label = tk.Label(self.text_frame, text="Sensor Location", font=("", self.fsize),
                                  bg=self.text_color)
        self.location_label.grid(row=0, column=2, sticky="we")

        self.blank = tk.PhotoImage(file="Blank.png")

        self.photo_label = tk.Label(self.text_frame, image=self.blank, bg=self.text_color)
        self.photo_label.grid(row=1, column=2, rowspan=3, sticky="nesw")

        # create dropdown list to select specific sensor
        self.sensor_list = []
        self.sensor_var = tk.StringVar()
        self.sensor_var.set("All")
        self.sensor_drop = tk.OptionMenu(self.text_frame, self.sensor_var, "All",
                                         *self.sensor_list, command=self.new_sensor)
        self.sensor_drop.grid(row=1, column=1)
        self.sensor_drop.configure(highlightthickness=0)

        # create radio buttons to determine whether the graph is 2D or 3D
        self.dim_var = tk.StringVar()
        self.dim_var.set("2D")
        self.radio_2D = tk.Radiobutton(self.text_frame, text="2D", bg=self.text_color, font=("", self.fsize),
                                       variable=self.dim_var, value="2D", command=self.update_canvas)
        self.radio_2D.grid(row=2, column=0)
        self.radio_3D = tk.Radiobutton(self.text_frame, text="3D", bg=self.text_color, font=("", self.fsize),
                                       variable=self.dim_var, value="3D", command=self.update_canvas)
        self.radio_3D.grid(row=2, column=1)

        # display the graph (initially empty)
        self.update_canvas()

    def get_type(self, type_unit):
        return type_unit.split("-")

    def set_dt(self, date):
        return datetime.datetime.strptime(date, "%Y%m%d")

    def ig(self):  # does nothing
        # used for buttons with no use
        pass

    def on_click(self, event):
        """
        :param event: determines which line on the graph is selected
        :return: None

        Description:
        This function is called every time a user clicks on a line on the graph.
        The parameter sent through can be used to determine which line has been
        selected. A seperate function is used to prevent multiple lines from being
        selected at once, whilst this function simply obtains the name of the line.
        """
        self.gid = event.artist.get_gid()  # name of the selected line
        self.t = time.time()

    def button_press(self, event):
        """
        :param event: required parameter with no actual use in this function
        :return: None

        Description:
        This function is called every time a user clicks anywhere on the canvas.
        This is called seperately from the on_click function to prevent multiple
        lines from being selected in one click. The function can only run if
        self.gid has a value of anything but None (anything where its bool()
        value is True)
        """
        # if the line is not selected already and self.gid has a value
        if self.sensor_var.get() != self.gid and self.gid:
            # change the selected sensor and update the graph
            self.sensor_var.set(self.gid)
            self.new_sensor(self.gid)
            # reset self.gid so its bool() value is false
        elif time.time() - self.t > 0.5 and self.gid:
            self.sensor_var.set("All")
            self.new_sensor("All")
            self.gid = None

    def new_day(self, week, day):
        """
        :param week: used to select correct row in calendar_buttons and button_ids variables
        :param day: as with week but selects the column
        :return: None

        Description:
        This function is called each time the user selects a new day from
        the calendar. It is used to change the colour of the buttons and
        obtain the data for the new date. This function allows for the
        currently selected type to remain selected so long as it exists
        for the new day as well.
        """
        print("---------------\n----New Day----\n---------------")
        # re-colour the buttons so the new day is now selected
        self.calendar_buttons[self.week][self.week_day].configure(bg="grey", relief="raised", state="normal")
        self.calendar_buttons[week][day].configure(bg="yellow", relief="sunken", state="disabled")
        # reset variables
        self.week = week
        self.week_day = day
        self.day = self.button_ids[week][day]
        self.current_date = datetime.datetime(self.year, self.month, self.day)
        if self.month_var:
            self.date_display.configure(text=str(self.year) + "-" + str(self.month))
        else:
            self.date_display.configure(text=self.current_date.date())

        print(week, day, self.day, self.current_date)
        print("")

        # reset type list
        query = {"timestamp": {"$gte": self.current_date, "$lt": self.current_date + self.delta}}
        self.type_list, self.unit_list = zip(*map(self.get_type,
                                                  self.db.records.find(query, {"type": 1}).distinct("type-unit")))
        print("Type List:")
        print(self.type_list)
        print("Unit List:")
        print(self.unit_list)
        # keep the currently selected type unless it no longer exists for the new date
        if self.type_var.get() not in self.type_list:
            self.type_var.set("None")
        # recreate the dropdown list for type
        self.type_drop["menu"].delete(0, "end")
        for i in self.type_list:
            action = partial(self.new_type, i)
            self.type_drop["menu"].add_command(label=i, command=action)

        self.sensor_var.set("All")
        if self.type_var.get() != "None":  # if the type didn't change
            query = {"timestamp": {"$gte": self.current_date, "$lt": self.current_date + self.delta},
                     "type": self.type_var.get()}
            self.sensor_list = self.db.records.find(query).distinct("sensor_id")
        else:
            self.sensor_list = []
        self.sensor_drop["menu"].delete(0, "end")
        for i in self.sensor_list:
            action = partial(self.new_sensor, i)
            self.sensor_drop["menu"].add_command(label=i, command=action)
        print("Sensor List:")
        print(self.sensor_list)
        self.photo_label.configure(image=self.blank)
        self.update_canvas()

    def new_month_year(self, direction, year):

        tt = time.time()
        f = open("info.csv", "r")
        contents = f.read().split("\n")
        f.close()
        lat_year = int(contents[0].split("-")[0])
        lat_month = int(contents[0].split("-")[1])

        if year:
            print("---------------\n---New  Year---\n---------------")
            self.year += direction
            if self.year < 2015:
                self.year = lat_year
            elif self.year > lat_year:
                self.year = 2015
            self.year_label.configure(text=str(self.year))
        else:
            print("---------------\n---New Month---\n---------------")
            self.month += direction
        if self.month <= 0 and self.year == lat_year:
            self.month = lat_month
        elif self.month <= 0 and self.year != lat_year:
            self.month = 12
        elif self.month > 12 or self.month > lat_month and self.year == lat_year:
            self.month = 1
        self.month_label.configure(text=str(self.month).zfill(2))

        self.day = 0
        counter = 0
        self.button_ids = calendar.monthcalendar(self.year, self.month)
        if len(self.button_ids) == 5:
            self.button_ids.append([0, 0, 0, 0, 0, 0, 0])

        tm = time.time()
        for week in range(6):
            for day in range(7):
                try:
                    date = datetime.datetime(self.year, self.month, self.button_ids[week][day])
                except ValueError:
                    date = 0
                counter += 1
                if self.button_ids[week][day] == 0:
                    self.calendar_buttons[week][day].configure(text="--", state="disabled",
                                                               bg="white", relief="raised")
                elif date in self.valid_dates and (self.day == 0 or self.month_var.get()):
                    self.day = self.button_ids[week][day]
                    self.week_day = day
                    self.week = week
                    txt = str(self.day)
                    self.calendar_buttons[week][day].configure(text=txt.zfill(2), state="disabled",
                                                               bg="yellow", relief="sunken")
                elif date in self.valid_dates and self.day != 0:
                    txt = str(self.button_ids[week][day])
                    self.calendar_buttons[week][day].configure(text=txt.zfill(2), state="normal",
                                                               bg="grey", relief="raised")
                else:
                    txt = str(self.button_ids[week][day])
                    self.calendar_buttons[week][day].configure(text=txt.zfill(2), state="disabled",
                                                               bg="red", relief="raised")

        tm = time.time() - tm

        if self.day > 0:
            self.current_date = datetime.datetime(self.year, self.month, self.day)
            print(self.current_date)
            if self.month_var:
                self.date_display.configure(text=str(self.year) + "-" + str(self.month))
            else:
                self.date_display.configure(text=self.current_date.date())

            self.collection = self.db[str(self.year) + str(self.month)]

            tq = time.time()
            query = {"timestamp": {"$gte": self.current_date, "$lt": self.current_date + self.delta}}
            self.type_list, self.unit_list = zip(*map(self.get_type,
                                                     self.db.records.find(query, {"type": 1}).distinct("type-unit")))
            print("Unit List:")
            print(self.unit_list)
            print("Query Time: " + str(time.time() - tq))
            print("Type List:")
            print(self.type_list)
            self.sensor_list = []

            if self.type_var.get() not in self.type_list:
                self.type_var.set("None")
            else:
                query = {"timestamp": {"$gte": self.current_date, "$lt": self.current_date + self.delta},
                         "type": self.type_var.get()}
                self.sensor_list = self.db.records.find(query).distinct("sensor_id")
            self.type_drop["menu"].delete(0, "end")
            for i in self.type_list:
                action = partial(self.new_type, i)
                self.type_drop["menu"].add_command(label=i, command=action)
            self.sensor_var.set("All")
            self.sensor_drop["menu"].delete(0, "end")
            for i in self.sensor_list:
                action = partial(self.new_sensor, i)
                self.sensor_drop["menu"].add_command(label=i, command=action)
            self.photo_label.configure(image=self.blank)

            print("Total Time: " + str(time.time() - tt))
            print("Button Time: " + str(tm))

            self.update_canvas()
        else:
            self.current_date = datetime.datetime(1, 1, 1)
            self.collection = None
            if self.month_var:
                self.date_display.configure(text=str(self.year) + "-" + str(self.month))
            else:
                self.date_display.configure(text=self.current_date.date())
            self.type_list = []
            self.unit_list = []
            print("Type List:")
            print(self.type_list)
            print("Unit List:")
            print(self.unit_list)
            self.sensor_list = []
            self.type_var.set("None")
            self.type_drop["menu"].delete(0, "end")
            for i in self.type_list:
                action = partial(self.new_type, i)
                self.type_drop["menu"].add_command(label=i, command=action)
            self.sensor_var.set("All")
            self.sensor_drop["menu"].delete(0, "end")
            for i in self.sensor_list:
                action = partial(self.new_sensor, i)
                self.sensor_drop["menu"].add_command(label=i, command=action)
            self.photo_label.configure(image=self.blank)

            print("Button Time: " + str(tm))
            print("Total Time: " + str(time.time() - tt))

            self.update_canvas()

    def select_month(self):
        if self.month_var.get():
            self.delta = relativedelta(months=1)
        else:
            self.delta = datetime.timedelta(days=1)
        self.new_month_year(0, False)

    def new_type(self, dtype):
        self.type_var.set(dtype)
        print("---------------\n---New  Type---\n---------------")
        if dtype == "None":
            self.sensor_list = []
        else:
            ts = time.time()
            query = {"timestamp": {"$gte": self.current_date, "$lt": self.current_date + self.delta},
                     "type-unit": dtype + "-" + self.unit_list[self.type_list.index(dtype)]}
            self.sensor_list = self.db.records.find(query).distinct("sensor_id")
            print("Query Time: " + str(time.time() - ts))
            print("Sensor List")
            print(self.sensor_list)
            print("Unit List:")
            print(self.unit_list)
        self.sensor_var.set("All")
        self.sensor_drop["menu"].delete(0, "end")
        for i in self.sensor_list:
            action = partial(self.new_sensor, i)
            self.sensor_drop["menu"].add_command(label=i, command=action)
        self.photo_label.configure(image=self.blank)
        self.update_canvas()

    def new_sensor(self, sensor):
        self.sensor_var.set(sensor)
        print("---------------\n--New  Sensor--\n---------------")
        if sensor == "All":
            self.photo_label.configure(image=self.blank)
        else:
            self.cursor.execute("SELECT sensor_lat, sensor_lng, sensor_height FROM sensors" +
                                " WHERE sensor_name = '" + sensor + "'")
            data = self.cursor.fetchall()[0]

            url = (self.base_url
                   + "markers=" + str(data[0]) + ","
                   + str(data[1]) +
                   "&size=200x200&format=PNG&zoom=12&key="
                   + self.api_key)
            f = open("Location.png", "wb")
            u = urlopen(url)
            f.write(u.read())
            u.close()
            f.close()
            self.image = tk.PhotoImage(file="Location.png")
            self.photo_label.configure(image=self.image)
        self.update_canvas()

    def save(self):
        """
        :param: None
        :return: None

        Description:
        This function is used to write the data from a currently selected graph
        to a file to be printed.
        2D graphs are simply written to a png file using matplotlibs savefig method.
        3D graphs are written to stl files by drawing a mesh based on the values and
        writing using the stl module. The structure is created from triangles to
        construct a 3d model of the graph. These triangles are structured so one is
        placed next to another to create quadralaterals (4 sides)
        """
        print("Saving Data to File")
        # 2D graph
        if self.dim_var.get() == "2D":
            # save figure to file: Graphs/year-month-day_type.png
            self.fig.savefig("Graphs/" + str(self.year) + "-" + str(self.month) + "-" + str(self.day)
                             + "_" + self.type_var.get() + ".png", format="png")
        # 3D graph
        else:
            total_graphs = []
            maxes = []
            # iterate through sensors to seperate lines
            for sensor_index in range(len(self.sensor_list)):
                # grab data from the pandas dataframe defined in update_canvas
                values = self.data[self.data.sensor_id == self.sensor_list[sensor_index]].drop("sensor_id", axis=1)
                # separate data into 2 lists of values and numerical timestamps
                value = list(np.array(values.value))
                timestamp = list(np.array(list(map(dates.date2num, values.index.to_pydatetime()))) -
                                 dates.date2num(self.current_date))

                # simple variables used to simplify code
                length = len(value)  # amount of data
                res = 8  # used to determine the size of the graph
                thickness = 0.2  # used to determine the distance between the front and back vertices

                maxes.append(max(timestamp) * res)

                # create verticies (corners) in format (x, y, z) within a single list called vertices
                # verticies on the bottom of the graph used for construction
                vertices_lower1 = np.array([
                    [x * res, sensor_index / 2, 0] for x in timestamp
                ])
                # vertices on the top to act as positions for the values
                vertices_upper1 = np.array([
                    [x * res, sensor_index / 2, z] for x, z in zip(timestamp, value)
                ])
                # as above but placed behind to create a thickness
                vertices_lower2 = np.array([
                    [x * res, sensor_index / 2 + thickness, 0] for x in timestamp
                ])
                vertices_upper2 = np.array([
                    [x * res, sensor_index / 2 + thickness, z] for x, z in zip(timestamp, value)
                ])
                # concatinate vertices into one list
                vertices1 = np.concatenate([vertices_lower1, vertices_upper1])
                vertices2 = np.concatenate([vertices_lower2, vertices_upper2])
                vertices = np.concatenate([vertices1, vertices2])

                # triangles used for faces are created using index references to the vertices list for each vertex
                # create all triangles with 2 vertices on the bottom for the front side
                faces_lower_front = np.array([
                    [vertex, vertex + 1, vertex + length]
                    for vertex in range(length - 1)
                ])
                # create all triangles with 1 vertex on the bottom for the front size
                faces_upper_front = np.array([
                    [vertex + length, vertex + length + 1, vertex + 1]
                    for vertex in range(length - 1)
                ])
                # as above but for the back
                faces_lower_back = np.array([
                    [vertex, vertex + 1, vertex + length]
                    for vertex in range(length * 2, length * 3 - 1)
                ])
                faces_upper_back = np.array([
                    [vertex + length, vertex + length + 1, vertex + 1]
                    for vertex in range(length * 2, length * 3 - 1)
                ])

                # concatenate front and back faces to one list
                faces_front_back = np.concatenate([faces_lower_front, faces_upper_front,
                                                   faces_lower_back, faces_upper_back])

                # faces on top of the graph between the front and back sides
                faces_top1 = np.array([
                    [vertex, vertex + 1, vertex + length * 2]
                    for vertex in range(length, length * 2 - 1)
                ])
                faces_top2 = np.array([
                    [vertex + 1, vertex + length * 2, vertex + length * 2 + 1]
                    for vertex in range(length , length * 2 - 1)
                ])
                # concatenate opposing triangles to one list
                faces_top = np.concatenate([faces_top1, faces_top2])

                # rectangle on the bottom of the graph
                faces_bottom = np.array([
                    [0, length - 1, length * 3 - 1],
                    [0, length * 2, length * 3 - 1]
                ])

                # rectangles on the sides of the graph
                faces_left = np.array([
                    [0, length, length * 2],
                    [length, length * 2, length * 3]
                ])
                faces_right = np.array([
                    [length - 1, length * 2 - 1, length * 3 - 1],
                    [length * 2 - 1, length * 3 - 1, length * 4 - 1]
                ])

                # cpncatenate all existing face lists to one list
                faces = np.concatenate([faces_front_back, faces_left, faces_right, faces_top, faces_bottom])
                graph = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
                for i, f in enumerate(faces):
                    for j in range(3):
                        graph.vectors[i][j] = vertices[f[j]]

                total_graphs.append(graph)

            mx = max(maxes)

            base_vertices = np.array([
                [-1, -1, 0],
                [-1, -1, -1],

                [mx + 1, -1, 0],
                [mx + 1, -1, -1],

                [-1, len(self.sensor_list) / 2 + 1, 0],
                [-1, len(self.sensor_list) / 2 + 1, -1],

                [mx, len(self.sensor_list) / 2 + 1, 0],
                [mx, len(self.sensor_list) / 2 + 1, -1],
            ])
            base_faces = np.array([
                [0, 2, 4],
                [2, 4, 6],

                [1, 3, 5],
                [3, 5, 7],

                [0, 1, 2],
                [1, 2, 3],

                [4, 5, 6],
                [5, 6, 7],

                [0, 1, 4],
                [1, 4, 6],

                [2, 3, 5],
                [3, 5, 7]
            ])

            base = mesh.Mesh(np.zeros(base_faces.shape[0], dtype=mesh.Mesh.dtype))
            for i, f in enumerate(base_faces):
                for j in range(3):
                    base.vectors[i][j] = base_vertices[f[j]]

            total_graphs.append(base)

            combined = mesh.Mesh(np.concatenate(list(g.data for g in total_graphs)))

            combined.save("Graphs/" + str(self.year) + "-" + str(self.month) + "-" + str(self.day)
                             + "_" + self.type_var.get() + ".stl")
        print("Data Saved")

    def update_canvas(self):
        d = threading.Thread(target=self.canvas_daemon)
        d.setDaemon(True)
        d.start()

    def canvas_daemon(self):
        print("---------------\n-Canvas Update-\n---------------")
        self.load_label.configure(text="Loading Graph", fg="orange", font=("", self.fsize, "bold"))
        self.type_drop.configure(state="disabled")
        self.sensor_drop.configure(state="disabled")
        self.save_button.configure(state="disabled")
        self.radio_2D.configure(state="disabled")
        self.radio_3D.configure(state="disabled")
        self.raise_year.configure(state="disabled")
        self.lower_year.configure(state="disabled")
        self.raise_month.configure(state="disabled")
        self.lower_month.configure(state="disabled")

        self.fig.clf()
        dimension = self.dim_var.get()
        if dimension == "2D":
            self.ax = self.fig.add_subplot(1, 1, 1)
        else:
            self.ax = self.fig.add_subplot(1, 1, 1, projection="3d")
        dtype = self.type_var.get()
        sensor = self.sensor_var.get()
        month = self.month_var.get()
        t = time.time()
        t2 = 0
        td = 0
        l = [0]

        if dtype == "None":
            self.canvas.draw()
            self.load_label.configure(text="----", fg="black", font=("", self.fsize))
        else:
            self.load_label.configure(text="Getting Data")
            self.units = self.unit_list[self.type_list.index(dtype)]
            over = None
            l = [] * len(self.sensor_list)
            td = time.time()
            if month:
                query = self.collection.find({"type-unit": dtype + "-" + self.units},
                                             {"timestamp": 1, "value": 1, "sensor_id": 1, "_id": 0})
            else:
                query = self.collection.find({"timestamp": {"$gte": self.current_date,
                                                            "$lt": self.current_date + self.delta},
                                              "type-unit": dtype + "-" + self.units},
                                             {"timestamp": 1, "value": 1, "sensor_id": 1, "_id": 0})
            self.data = pandas.DataFrame.from_records(query)
            self.data = self.data.set_index("timestamp").sort_index(ascending=True)
            td = time.time() - td

            random.seed(time.time())
            if random.random() < 0.1:
                self.waste_time()

            random.seed(10)
            self.load_label.configure(text="Plotting Data")
            # display 2D graph
            if self.dim_var.get() == "2D":
                self.save_button.configure(text="Save as png")
                for sensor_name in self.sensor_list:
                    t1 = time.time()

                    values = self.data[self.data.sensor_id == sensor_name].drop("sensor_id", axis=1)

                    if sensor == sensor_name:
                        over = [values, (random.random(), random.random(), random.random())]
                    elif sensor == "All":
                        self.ax.plot(values, linewidth=0.5, picker=2, gid=sensor_name,
                                     color=(random.random(), random.random(), random.random()))
                    else:
                        self.ax.plot(values, linewidth=0.5, picker=2, gid=sensor_name,
                                     color=(0.5, 0.5, 0.5))
                        (random.random(), random.random(), random.random())
                    l.append(time.time() - t1)

                if over:
                    self.ax.plot(over[0], linewidth=2, picker=2, gid=sensor,
                                 color=(over[1]))

                self.load_label.configure(text="Displaying Data")
                t2 = time.time()
                delta = datetime.timedelta(days=1)
                plt.xlim(datetime.datetime(self.year, self.month, self.day),
                         datetime.datetime(self.year, self.month, self.day) + delta)
                plt.ylabel("Y: " + dtype + " / " + str(self.units))
                if month:
                    plt.xlabel("X: Day")
                else:
                    plt.xlabel("X: Time / Hours")

                self.ax.xaxis.set_major_formatter(dates.DateFormatter('%H'))

                self.ax.set_title(dtype)
                self.canvas.draw()
                t2 = time.time() - t2
                self.load_label.configure(text="Graph Loaded", fg="#50FF50", font=("", self.fsize))
            # display 3D graph
            else:
                self.save_button.configure(text="Save as stl")
                for sensor_index in range(len(self.sensor_list)):
                    t1 = time.time()
                    values = self.data[self.data.sensor_id == self.sensor_list[sensor_index]].drop("sensor_id", axis=1)
                    value = list(values.value)
                    timestamp = list(map(dates.date2num, values.index.to_pydatetime()))

                    if sensor == self.sensor_list[sensor_index]:
                        self.ax.plot(timestamp, [sensor_index for _ in range(len(timestamp))], zs=value, zdir="z",
                                     color=(random.random(), random.random(), random.random()),
                                     linewidth=2, picker=2, gid=self.sensor_list[sensor_index],)
                    elif sensor == "All":
                        self.ax.plot(timestamp, [sensor_index for _ in range(len(timestamp))], zs=value, zdir="z",
                                     color=(random.random(), random.random(), random.random()),
                                     linewidth=1, picker=2, gid=self.sensor_list[sensor_index],)
                    else:
                        self.ax.plot(timestamp, [sensor_index for _ in range(len(timestamp))], zs=value, zdir="z",
                                     color=(0.5, 0.5, 0.5),
                                     linewidth=1, picker=2, gid=self.sensor_list[sensor_index],)
                        (random.random(), random.random(), random.random())

                    l.append(time.time() - t1)

                self.load_label.configure(text="Displaying Data")
                t2 = time.time()
                delta = datetime.timedelta(days=1)
                plt.xlim(datetime.datetime(self.year, self.month, self.day),
                             datetime.datetime(self.year, self.month, self.day) + delta)
                plt.ylim(auto=True)
                self.ax.set_zlabel("Z: " + dtype + " / " + str(self.units))
                self.ax.set_xlabel("X: Time / Hours")
                self.ax.set_ylabel("Y: Sensors")

                plt.yticks([i for i in range(0, len(self.sensor_list))], self.sensor_list, fontsize=5)
                self.ax.xaxis.set_major_formatter(dates.DateFormatter('%H'))

                self.ax.set_title(dtype)
                self.canvas.draw()
                t2 = time.time() - t2
                self.load_label.configure(text="Graph Loaded", fg="#50FF50", font=("", self.fsize))

        print("Total Time: " + str(time.time() - t))
        print("Data Load: " + str(td))
        print("Mean Data Plot: " + str(statistics.mean(l)) + "  Over " + str(len(self.sensor_list)) + " sensors")
        print("Total Data Plot: " + str(statistics.mean(l) * len(self.sensor_list)))
        print("Display Graph: " + str(t2))

        self.type_drop.configure(state="normal")
        self.sensor_drop.configure(state="normal")
        self.save_button.configure(state="normal")
        self.radio_2D.configure(state="normal")
        self.radio_3D.configure(state="normal")
        self.raise_year.configure(state="normal")
        self.lower_year.configure(state="normal")
        self.raise_month.configure(state="normal")
        self.lower_month.configure(state="normal")

    def waste_time(self):
        options = ["Wasting time", "Doing nothing", "Doing everything", "Drinking coffee", "Crying over spilled milk",
                   "Pondering", "Eating KFC", "Eating milk", "Eating the floor",
                   "Willfully deciding to waste your time", "The universe",
                   "Wasting everything", "Wasting so much it gets wasted", "Life",
                   "Ikki Ikki IKKI FRRtang ZIMboing mpfmflpmf"]
        # for i in range(random.randint(1, 4)):
        #     self.load_label.configure(text=random.choice(options))
        #     time.sleep(random.randint(1, 3))


user32 = ctypes.windll.user32
SCREEN_SIZE = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

app = RootApplication()
