import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
import tkinter as tk
import numpy as np
import random


root = tk.Tk()

fig = plt.figure(1)
plt.ion()
t = np.arange(0.0,3.0,0.01)
s = np.sin(np.pi*t)
plt.plot(t,s)

canvas = matplotlib.backends.backend_tkagg.FigureCanvasTkAgg(fig, master=root)


def update():
    s = np.cos(np.pi*t)
    plt.plot(t,s)

    fig.canvas.draw()

plot_widget.grid(row=0, column=0)
tk.Button(root,text="Update",command=update).grid(row=1,column=0)
root.mainloop()