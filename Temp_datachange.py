import pymongo

client = pymongo.MongoClient()
db = client.big_database
records = db.records
count = 0
for i in records.find():
    ts = i["timestamp"].split(" ")
    collection = db[ts[0]]
    collection.update_one(
        {
            "timestamp": i["timestamp"],
            "value": i["value"],
            "units": i["units"],
            "type": i["type"],
            "sensor_id": i["sensor_id"]
        }, {"$set": {
            "timestamp": i["timestamp"],
            "value": i["value"],
            "units": i["units"],
            "type": i["type"],
            "sensor_id": i["sensor_id"]
        }}, upsert=True)
    count += 1
    print(count)
