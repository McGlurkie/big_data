from urllib.request import urlopen
import json

class UrbanObservatoryAPI:

    def __init__(self, api_key):
        self.base_url = 'http://uoweb1.ncl.ac.uk/api/v1/sensors/data/raw.json?'
        self.api_key = api_key
        self.data = []

    def read_from_api(self, query, file_to_write):

        print(self.base_url+query + '&api_key=' + self.api_key)

        json_data = urlopen(self.base_url+query + '&api_key=' + self.api_key).read().decode("utf-8")

        if file_to_write:
            f = open(file_to_write, 'w')
            f.write(str(json_data))
            f.close()

        self.data = json.loads(str(json_data))

        return self

    def raw(self):
        return self.data

    def read_from_file(self, file_to_read):

        self.data = json.load(file_to_read)


        return self

    def parse_data(self):

        the_data = []

        for sensor in self.data:

            if sensor['geom']['type'] == 'Point':

                for dataitem in sensor['data']:

                    for datekey in sensor['data'][dataitem]['data']:
                        the_data.append({
                            'sensor_id': sensor['name'],
                            'timestamp': datekey,
                            'value': sensor['data'][dataitem]['data'][datekey],
                            'lat': sensor['geom']['coordinates'][1],
                            'lng': sensor['geom']['coordinates'][0],
                            'type': dataitem,
                            'units': sensor['data'][dataitem]['meta']['units']

                        })

        return the_data
