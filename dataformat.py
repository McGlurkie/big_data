from json import loads  # used to extract data in json / dictionary format
from datetime import datetime, timedelta  # used to work with dates
from sqlite3 import connect  # used to store and manipulate sensor data with sql
import time
from pymongo import MongoClient  # used to store and manipulate record data with sql
from sys import version, exit  # used to detect version and errors
import pandas
from pprint import pprint  # used in debugging

# access correct version of urllib based on version of python
if version[0] == "2":
    from urllib import urlopen
elif version[0] == "3":
    from urllib.request import urlopen
else:  # if incompatible python version in use
    print("version " + version[0] + " of python is incompatible. Please use version 2 or 3")
    exit()


# used as an object to allow for this module to be easily imported and structured
class DataStructure(object):
    # called upon creation
    def __init__(self, database="mongodb://pi:3.14159@10.70.39.148/big_database"):
        # base url where the data will be accessed from
        self.base_url = "http://uoweb1.ncl.ac.uk/api/v1/sensors/data/raw.json?"
        # api key to allow access to the data
        self.api_key = "j9j780sqlfx7am0sgdtxcz2gbs7nuddfvksvhg17kycz0fv3cuwgy9pir2bdaw91mi7dmyqzbridg3mxkixf2w1nlp"
        self.data = []
        self.client = MongoClient(database)  # access mongodb server
        self.db = self.client.big_database  # access database in the server

    # function to add 1 day of data
    def add_data(self, date, do_print=True):
        """
        :param date: Must be a string datetime in the format YYYYMMDD (no delimiter)
        :param do_print: Determines if results will be printed to the console
        :return: Time taken to complete the function

        Note: Several print statements have been used in this function,
              these are used to identify areas where problems may occur in
              adding the data.

        Description: This function will take the parameter date and
                     access data from the Urbain Observatory api for
                     the given day. This data will then be stored in
                     a MongoDB server and an SQL database file in the
                     format:
                     SQL:

                     MongoDB:

        """
        self.start = time.time()  # save start time for the function
        try:  # access urbain observatory api to obtain data for the given date
            # separate date into its components
            year = date[:4]
            month = date[4:6]
            day = date[6:8]
            # format date into datetime object
            self.sdt = datetime(int(year), int(month), int(day))
            # increment date to obtain a final point where data is not needed
            self.edt = self.sdt + timedelta(days=1)
            # time period to ask for data
            times = ("start_time=" + date + "&end_time=" + self.edt.strftime("%Y%m%d"))
            # open url and read contents as a utf-8 formatted json dictionary
            self.data = loads(str(urlopen(self.base_url + times
                                          + '&api_key='
                                          + self.api_key).read().decode("utf-8")))
        except Exception as e:  # usually occurs when the website is unable to give the data
            if do_print:
                print("Data not found: " + date)
                print("Reason: " + str(e))
        else:

            # name of the sql table
            if do_print:
                # notify user of the successful connection to the website and time taken
                print("Connected to website    Time: "
                      + str(int(time.time() - self.start)) + "    date: " + date)
            end = time.time()  # time before accessing sql
            # connect to sql database stored in Data.db file
            conn = connect("Data.db")
            cursor = conn.cursor()
            # recreates the table with given parameters
            create = (
                "CREATE TABLE IF NOT EXISTS sensors (" +
                "sensor_id INT AUTO INCREMENT," +
                "sensor_name VARCHAR(255) NOT NULL," +
                "sensor_height FLOAT(12,12) NOT NULL," +
                "sensor_lat FLOAT(15,15) NOT NULL," +
                "sensor_lng FLOAT(15,15) NOT NULL," +
                "PRIMARY KEY (sensor_id)"
                "UNIQUE (sensor_name));"
            )
            cursor.execute(create)  # execute string query as sql command

            sensor_list = []  # list of all sensor information for the day
            for box in self.data:  # iterate through every sensor from the api
                name = box["name"]  # name of the sensor (primary key
                # global location of the sensor
                height = box["base_height"]
                lat = box["geom"]["coordinates"][1]
                lng = box["geom"]["coordinates"][0]
                # append tuple of obtained values to sensor_list
                sensor_list.append((name, height, lat, lng))
            sql_count = len(sensor_list)  # used to keep track of how many sensors were added

            # bulk execute INSERT INTO command with parameters from sensor_list
            cursor.executemany(
                "INSERT OR IGNORE INTO sensors (sensor_name,sensor_height,sensor_lat,sensor_lng)"
                + "VALUES (?,?,?,?);", sensor_list)
            conn.commit()  # save changes to the database

            sql_time = time.time() - end  # time taken to input data into the sql database
            conn.close()  # connection to sql database is no longer needed
            if do_print:
                # notify user of success with storing data in the sql database, time taken and no. updates
                print("Updated SQL sensor data  " +
                      "    Time: " + str(int(sql_time)) +
                      "    Updates: " + str(sql_count))

            end = time.time()  # time before accessing MongoDB
            pymo_count = 0  # used to keep track of how many records are added

            ins = []
            stamp = []
            for box in self.data:
                for sensor in box["data"]:
                    for record in box["data"][sensor]["data"]:
                        pymo_count += 1
                        ins.append({
                            "timestamp": datetime.strptime(record, "%Y-%m-%d %H:%M:%S"),
                            "value": box["data"][sensor]["data"][record],
                            "type-unit": str(sensor) + "-" + str(box["data"][sensor]["meta"]["units"]),
                            "sensor_id": box["name"]
                        })
                        stamp.append(record)

            try:
                if date[:6] not in self.db.collection_names():
                    self.db.create_collection(date[:6])
                self.collection = self.db[date[:6]]
                indexes = self.collection.index_information()
                if "timestamp_1" not in indexes:
                    self.collection.create_index([("timestamp", 1)])
                if "sensor_id_1_type-unit_1" not in indexes:
                    self.collection.create_index([("sensor_id", 1), ("type-unit", 1)])

                self.collection.delete_many({"timestamp": {"$in": stamp}})
                self.collection.insert_many(ins)

                pymo_time = time.time() - end

                if do_print:
                    print("Created pymo records    Time: " + str(int(pymo_time)) +
                          "    Updates: " + str(pymo_count))
                    print("Average SQL Time: " + ("%s" % float("%.2g" % (sql_time / sql_count))) +
                          "    Average pymo Time: " + ("%s" % float("%.2g" % (pymo_time / pymo_count))))

            except Exception as e:
                if do_print:
                    print("pymo Error:", e)
            else:
                f = open("ValidDates.csv", "r")
                contents = f.read().split(",")
                f.close()
                if date not in (contents):
                    contents.append(date)
                    f = open("ValidDates.csv", "w")
                    f.write(",".join(contents))
                    f.close()
        return int(time.time() - self.start)

    # function used to add multiple days of data
    def add_cluster(self, start, end):
        """
        :param start: Must be a string datetime in the format YYYYMMDD (no delimiter)
                      Used to determine the first day to be added
        :param end: Same as start.
                    Used to determine the final day to be added
        :return None:
        """
        ttime = 0
        first = datetime(int(start[:4]), int(start[4:6]), int(start[6:8]))
        last = datetime(int(end[:4]), int(end[4:6]), int(end[6:8]))
        while first <= last:
            date = str(first.year).zfill(4) + str(first.month).zfill(2) + str(first.day).zfill(2)
            ctime = self.add_data(date, True)
            print("Day " + date + " took " + str(int(ctime)) + " seconds total\n")
            ttime += ctime
            first += timedelta(days=1)
        print("Total time taken: " + str(int(ttime)))
