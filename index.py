# import modules
from UrbanObservatoryAPI import UrbanObservatoryAPI
from matplotlib import pyplot as plt
import random
import math


class DataStruct(object):
    def __init__(self):
        self.tables = []

    def add_table(self):
        self.tables.append(Table())


class Table(object):
    def __init__(self):
        self.IDs = {}
        self.data = []


class record(object):
    def __init__(self, timestamp, lat, lng, dtype, sensor_id, value):
        self.timestamp = timestamp
        self.lat = lat
        self.lng = lng
        self.type = dtype
        self.sensor_id = sensor_id
        self.value = value


# used to add axis to screen
def add_plot(plot_list, position, title, s):
    # used to obtain the same colours every time without defining the quantity required
    random.seed(365)
    # seperate data from the plot_list into x and y coordinate listgs
    x = []
    y = []
    for i in plot_list:
        x.append([])
        y.append([])
        try:
            for j in plot_list[i]:
                x[-1].append(j["timestamp"].split(" ")[1].split(":")[1])
                y[-1].append(j["value"])
        except TypeError:
            x = x[:-1]
            y = y[:-1]
    # create axis and plot lines separated by record id
    ax = fig.add_subplot(s, s, position)
    for i in range(len(x)):
        x[i], y[i] = zip(*sorted(zip(x[i], y[i])))
        ax.plot(x[i], y[i], linewidth=0.5,
                color=(random.randint(0, 100) / 100, random.randint(0, 100) / 100, random.randint(0, 100) / 100))
    # set labels and title
    plt.xlabel("time / s")
    ax.set_title(title)


# used to carry out events corresponding to user pressed keys
def key_down(event):
    global fig, length, size, large, graph, line
    print(event.key, large)

    # display graph corresponding to number entered by user
    if event.key in (str(i) for i in range(1, length + 1)):
        fig.clf()
        for i in data_struct:
            if data_struct[i]["ID"] == int(event.key):
                add_plot(data_struct[i], 1, i, 1)
        large = True
        print("asdf")
        graph = int(event.key)
        line = 0
        plt.show()
    # reset screen to default display for all graphs
    elif event.key == "escape":
        fig.clf()
        counter = 0
        for i in data_struct:
            counter += 1
            add_plot(data_struct[i], counter, i, size)
            data_struct[i]["ID"] = counter
        large = False
        graph = 0
        line = 0
        plt.show()
    # change which line is being displayed
    elif event.key == "up" and large:
        print(line)
        if line == length:
            line = 0
            for i in data_struct:
                if data_struct[i]["ID"] == int(event.key):
                    add_plot(data_struct[i], 1, i, 1)
        else:
            line += 1
            fig.clf()
            for i in data_struct:
                if data_struct[i]["ID"] == graph:
                    for j in data_struct[i]:
                        if type(data_struct[i][j]) != type(1):
                            print(type(data_struct[i][j][0]["num_id"]),data_struct[i][j][0]["num_id"])
                            if data_struct[i][j][0][-1] == line:
                                print("asdf", [data_struct[i][j]])
                                add_plot([data_struct[i][j]], 1, i, 1)
            plt.show()


# define common variables
large = False
graph = 0
line = 0

# obtain data
api_key = "j9j780sqlfx7am0sgdtxcz2gbs7nuddfvksvhg17kycz0fv3cuwgy9pir2bdaw91mi7dmyqzbridg3mxkixf2w1nlp"

api = UrbanObservatoryAPI(api_key)

data = api.read_from_api("start_time=20150117120000&end_time=20150117180000&sensor_type=Weather",
                         'temp.json').parse_data()
print(data)

# reformat data to a more easy to use form
struct = {}
for i in data:
    if i["type"] not in struct:
        struct[i["type"]] = []
    struct[i["type"]].append(i)

print(type(struct), struct)
data_struct = DataStruct()
for i in struct:
    data_struct.add_table(i)
    for j in struct[i]:
        if j["sensor_id"] not in data_struct.tables[-1].IDs:
            data_struct.tables[-1].IDs.append(j["sensor_id"])
        data_struct[i][j["sensor_id"]].append(j)

# data_struct{data_type{sensor_id[sensed_data{data_attributes}]}}
for i in data_struct:
    print(i, data_struct[i])

# determine structure of the graphs
length = len(data_struct)
size = math.trunc(math.sqrt(length)) + 1
# create figure to display the graphs on
fig = plt.figure()

# iterate through and display all items in data_struct
counter = 0
for i in data_struct:
    counter += 1
    add_plot(data_struct[i], counter, i, size)
    data_struct[i]["ID"] = counter

# check for user input
cid = fig.canvas.mpl_connect('key_press_event', key_down)
# setup screen
plt.tight_layout()
plt.legend()
plt.show()
